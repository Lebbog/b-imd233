$(function() {
    window.sr = ScrollReveal();
    //Slide paragraphs of text from top, 300 pixels down
    var para = document.querySelectorAll('.lead');
    sr.reveal(para, {
        duration: 1750,
        origin: 'top',
        distance: '300px',
        reset: true
    });
    //Slide jumbotron from top, 300 pixels down
    var jumb = document.querySelectorAll('.jumbotron2');
    sr.reveal(jumb, {
        duration: 1750,
        origin: 'top',
        distance: '300px',
        reset: true
    });
    //Slide images from the right, 300 pixels to the left
    var img = document.querySelectorAll('img');
    sr.reveal(img, {
        duration: 1750,
        origin: 'right',
        distance: '300px',
        reset: true
    });
    //Slide footer from the left, 300 pixels to the right
    var foot = document.querySelectorAll('footer');
    sr.reveal(foot, {
        duration: 1750,
        origin: 'left',
        distance: '300px',
        viewFactor: 0.2,
        reset: true
    });
    sr.reveal('.specs-right', {
        duration: 1750,
        origin: 'right',
        distance: '300px',
        viewFactor: 0.2
    });
    //Slide buttons from the left, 300 pixels to the right
    var button = document.querySelectorAll('button');
    sr.reveal(button, {
        duration: 1750,
        origin: 'left',
        distance: '300px',
        viewFactor: 0.2,
        reset: true
    });
    //Slide buttons from the left, 300 pixels to the right
    var links = document.querySelectorAll('a');
    sr.reveal(links, {
        duration: 1750,
        origin: 'left',
        distance: '300px',
        viewFactor: 0.2,
        reset: true
    });
    //Slide headers from the bottom, 300 pixels to the top
    var headers = document.querySelectorAll('h1');
    sr.reveal(headers, {
        duration: 1750,
        origin: 'bottom',
        distance: '300px',
        viewFactor: 0.2,
        reset: true
    });
});

// $(".button").hover(function() {
//     $(this).children("img").fadeTo(200, 0.25)
//         .end().children(".hover").show();
// }, function() {
//     $(this).children("img").fadeTo(200, 1)
//         .end().children(".hover").hide();
// });