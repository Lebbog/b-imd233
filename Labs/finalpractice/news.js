$(function() {
    var url = 'https://newsapi.org/v2/top-headlines?' +
        'country=us&' +
        'apiKey=cd081ac64b6044c6ba98bf6f807c7140';
    var req = new Request(url);
    fetch(req)
        .then(function(response) {
            //Response now contains json
            return response.json();
        })
        .then(function(results) {
            var element = document.getElementById("stories");
            element.innerHTML = "";
            for (var i = 0; i < results.articles.length; i++) {
                var urlImg = results.articles[i]['urlToImage'];
                var url = results.articles[i]['url'];
                var title = results.articles[i]['title'];
                var desc = results.articles[i]['description'];
                var source = results.articles[i]['source']['name'];
                var published = results.articles[i]['publishedAt'];
                element.innerHTML += "<div class=\"blog-post\">" +
                    "<h4 class=\"blog-post-title \">" + title + "</h4>" +
                    "<img class=\"img-responsive\" src=\"" + urlImg + "\" height=\"400\">" +
                    "<h6 class=\"blog-post-info \">" + desc + "</h6>" +
                    "<p class=\"blog-post-meta\"> Source: " +
                    "<a href=\"" + source + "\">" + source + "</a>" +
                    " published at " + published + "</p>" +
                    "<a href=\"" + url + "\"target=_\"_blank\"> Read More" + "</a> </div> <hr class=\"featurette-divider\">";
            }
        })
});