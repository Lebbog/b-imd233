// Shorthand for $( document ).ready()
$(function() {
    // weather update button click
    $('button').on('click', function(e) {
        $('ul li').each(function() {
            console.log("this:" + this);
            $(this).remove();
        });
        $.ajax({
            processing: false,
            url: "http://api.wunderground.com/api/f82417e113e42a7b/geolookup/conditions/q/WA/Bothell.json",
            dataType: "jsonp",
            beforeSend: function() {
                //prevent multiple requests 
                processing = true;
            },
            success: function(parsed_json) {
                if (processing) {
                    var city = parsed_json['location']['city'];
                    var state = parsed_json['location']['state'];
                    var temp_f = parsed_json['current_observation']['temp_f'];
                    var rh = parsed_json['current_observation']['relative_humidity'];
                    var str = "<li> Location : " + city + ", " + state + "</li>";
                    console.log("Current temperature in " + city + " is: " + temp_f);

                    // Update list items with data from above...
                    $('ul').append(str);
                    $('ul li:last').attr('class', 'list-group-item');
                    str = "<li> Current temperature: " + temp_f + " &#x2109</li>";
                    $('ul').append(str);
                    $('ul li:last').attr('class', 'list-group-item');
                    str = "<li> Relative Humidity: " + rh + "</li>";
                    $('ul').append(str);
                    $('ul li:last').attr('class', 'list-group-item');


                    //Weather description and Icon
                    var weath = parsed_json['current_observation']['weather'];
                    str = "<li> Weather Description: " + weath + "&emsp;<img src=\"" + parsed_json['current_observation']['icon_url'] + "\"></li>";
                    $('ul').append(str);
                    $('ul li:last').attr('class', 'list-group-item');

                    //Wind description
                    var wind = parsed_json['current_observation']['wind_string'];
                    str = "<li> Wind Condition: " + wind + "</li>";
                    $('ul').append(str);
                    $('ul li:last').attr('class', 'list-group-item');

                    processing = false;
                }
            }
        });
    });
});