function flight(airline, number, origin, destination, dep_time, arrival_time, arrival_gate) {
    this.airline = airline;
    this.number = number;
    this.origin = origin;
    this.destination = destination;
    this.dep_time = dep_time;
    this.arrival_time = arrival_time;
    this.arrival_gate = arrival_gate;
    this.getDuration = function() { //Get duration of flight in ms
        var departure = new Date(dep_time);
        var arrival = new Date(arrival_time); //Time of flight arrival
        var time = arrival.getTime() - departure.getTime(); //time is now in milliseconds

        var seconds = time / 1000;
        var hours = parseInt(seconds / 3600);
        seconds = seconds % 3600; // seconds remaining after extracting hours
        var minutes = parseInt(seconds / 60); // 60 seconds in 1 minute
        seconds = seconds % 60;
        //Trivial way of padding 0 
        seconds = (seconds < 10) ? "0" + seconds : seconds;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        hours = (hours < 10) ? "0" + hours : hours;
        return (hours + ":" + minutes + ":" + seconds);
    }
}
var flight1 = new flight("Air Transat", 122, "Toronto, Canada", "London, UK", "May 14, 2018 10:39:00", "May 15, 2018 10:08:00", "Terminal S");
var flight2 = new flight('SkyWest', 3419, 'Minneapolis, MN', 'San Francisco, CA', "May 15, 2018 03:36:00", "May 15, 2018 05:09:00", 'S5');
var flight3 = new flight('American Airlines', 2090, 'Charlotte, NC', 'Denver, CO', "May 15, 2018 05:04:00", "May 15, 2018 06:37:00", 'A49');
var flight4 = new flight('United', 481, 'Denver, CO', 'Los Angeles, CA', "May 15, 2018 03:25:00", "May 15, 2018 04:38:00", 'C2');
var flight5 = new flight('Air Canada Rouge', 1897, 'Las Vegas, NV', 'Vancouver, Canada', "May 15, 2018 01:34:00", "May 15, 2018 04:26:00", 'E76');
var ar = [flight1, flight2, flight3, flight4, flight5];

var table = document.getElementsByTagName("tbody").item(0); //grab first <tbody> encountered
for (var i = -1; i < ar.length; i++) {
    if (i == -1) {
        table.innerHTML = "<th>Airline</th> <th>Number</th> <th>Origin</th> <th>Destination</th> <th>Departure Time</th> <th>Arrival Time</th> <th>Arrival Gate</th> <th>Time in Flight</th>";
    } else {
        table.innerHTML += "<tr><td>" + ar[i]["airline"] + "</td><td>" + ar[i]["number"] + "</td><td>" + ar[i]["origin"] +
            "</td><td>" + ar[i]["destination"] + "</td><td>" + ar[i]["dep_time"] + "</td><td>" + ar[i]["arrival_time"] + "</td><td>" + ar[i]["arrival_gate"] +
            "</td><td>" + ar[i].getDuration() + "</td></tr";
    }
}