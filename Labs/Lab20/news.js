$(function() {
    var url = 'https://newsapi.org/v2/top-headlines?' +
        'country=us&' +
        'apiKey=cd081ac64b6044c6ba98bf6f807c7140';
    var req = new Request(url);
    fetch(req)
        .then(function(response) {
            //Response now contains json
            return response.json();
        })
        .then(function(results) {
            $("tbody").append("<th>News Source</th><th>Title</th><th>Read More</th>");

            var source, title, link, linkImg;
            for (var i = 0; i < results.articles.length; i++) {
                source = results.articles[i]["source"]["name"];
                title = results.articles[i]["title"];
                link = results.articles[i]["url"];
                linkImg = results.articles[i]["urlToImage"];
                $('tbody').append("<tr><td>" + source + "</td><td> " + title + "</td><td><a href = " + link + ">Link</a></td></tr>");
            }
        })
});