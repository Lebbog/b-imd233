var stockData = [{
        name: "Microsoft",
        marketCap: "$381.7B",
        sales: "$86.8B",
        profit: "$22.1B",
        numOfEmplyoee: "128,000"
    },
    {
        name: "Symetra Financial",
        marketCap: "$2.7B",
        sales: "$2.2B",
        profit: "$254.4B",
        numOfEmplyoee: "1,400"
    },
    {
        name: "Micron Technology",
        marketCap: "$37.6B",
        sales: "$16.4B",
        profit: "$3.0B",
        numOfEmplyoee: "30,400"
    },
    {
        name: "F5 Networks",
        marketCap: "$9.5B",
        sales: "$1.7B",
        profit: "$311.2M",
        numOfEmplyoee: "3,498"
    },
    {
        name: "Expedia",
        marketCap: "$10.8B",
        sales: "$5.8B",
        profit: "$398.2M",
        numOfEmplyoee: "2,344"
    },
    {
        name: "Nautilus",
        marketCap: "$9.5B",
        sales: "$1.7B",
        profit: "$311.2M",
        numOfEmplyoee: "5,652"
    },
    {
        name: "Heritage Financial",
        marketCap: "$3.5B",
        sales: "$1.4B",
        profit: "$311.2M",
        numOfEmplyoee: "5004"
    },
    {
        name: "Cascade Microtech",
        marketCap: "$4.5B",
        sales: "$145.76M",
        profit: "$311.2M",
        numOfEmplyoee: "1,348"
    },
    {
        name: "Nike",
        marketCap: "$9.5B",
        sales: "$1.7B",
        profit: "$311.2M",
        numOfEmplyoee: "7,544"
    },
    {
        name: "Alaska Air Group",
        marketCap: "$7.9B",
        sales: "$1.7B",
        profit: "$311.2M",
        numOfEmplyoee: "2,054"
    }
];
function myFunc(item,index)
{
    var table = document.getElementsByTagName("tbody").item(0);
    if (index == 0) {
        table.innerHTML = "<th>Company</th> <th>Market Cap</th> <th>Sales</th> <th>Profits</th> <th>Employees</th>";
        table.innerHTML += "<tr><td>" + item["name"] + "</td><td>" + item["marketCap"] + "</td><td>" + item["sales"] + "</td><td>" + item["profit"] + "</td><td>" + item["numOfEmplyoee"] + "</td></tr>";
    } else {
        table.innerHTML += "<tr><td>" + item["name"] + "</td><td>" + item["marketCap"] + "</td><td>" + item["sales"] + "</td><td>" + item["profit"] + "</td><td>" + item["numOfEmplyoee"] + "</td></tr>";
    }                       
}