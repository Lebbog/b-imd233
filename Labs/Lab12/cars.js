var carRay = new Array(5); //Holds arrays of car models
//Make, model, year, price
var car1 = ["Ford", "F1-50", "1990", "$5,000"];
var car2 = ["BMW", "3 Series", "2002", "$27,100"];
var car3 = ["Toyota", "Camry", "2002", "$18,970"];
var car4 = ["Jetta", "GT", "1997", "$3,000"];
var car5 = ["Ford", "Fiesta", "2010", "$8,000"];
carRay[0] = car1;
carRay[1] = car2;
carRay[2] = car3;
carRay[3] = car4;
carRay[4] = car5;

var table = document.getElementsByTagName("tbody").item(0); //grab first <tbody> encountered

for (var i = 0; i < carRay.length; i++) {
    var row = document.createElement("tr");
    for (var j = 0; j < carRay[0].length; j++) {
        var cell = document.createElement("td");
        var textnode = document.createTextNode(carRay[i][j]);
        cell.appendChild(textnode);
        row.appendChild(cell);
    }
    table.appendChild(row);
}