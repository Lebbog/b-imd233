function calcCircleGeometries(radius) {
    var area = Math.PI * radius * radius;
    var circumference = 2 * Math.PI * radius;
    var diameter = 2 * radius;
    //Code for filling table
    var table = document.getElementsByTagName("tbody").item(0);
    var row = document.createElement("tr");
    for (var i = 0; i < 3; i++) {
        var cell = document.createElement("td");

        var data;
        switch (i) {
            case 0:
                {
                    data = area;
                    break;
                }
            case 1:
                {
                    data = circumference;
                    break;
                }
            case 2:
                {
                    data = diameter;
                    break;
                }
        }
        var textnode = document.createTextNode(data);
        cell.appendChild(textnode);
        row.appendChild(cell);
    }
    table.appendChild(row);
}
calcCircleGeometries(Math.floor(Math.random() * 10));
calcCircleGeometries(Math.floor(Math.random() * 10));
calcCircleGeometries(Math.floor(Math.random() * 10));