var wx_data = [{ day: "fri", hi: 82, lo: 55 },
    { day: "sat", hi: 75, lo: 52 },
    { day: "sun", hi: 69, lo: 52 },
    { day: "mon", hi: 69, lo: 48 },
    { day: "tue", hi: 68, lo: 51 }
];

var avg = wx_data.reduce(function(val, d, i, ar) {
    val.hi += d.hi;
    val.lo += d.lo;
    if (i == ar.length - 1) {
        val.hi /= 5;
        val.lo /= 5;
        return val;
    } else {
        return val;
    }
})
var table = document.getElementsByTagName("tbody").item(0); //grab first <tbody> encountered


for (var i = -1; i < wx_data.length; i++) {
    if (i == -1) {
        table.innerHTML = "<th>Day</th> <th>High</th> <th>Low</th>";
    } else {
        table.innerHTML += "<tr><td>" + wx_data[i]["day"] + "</td><td>" + wx_data[i]["hi"] + "</td><td>" + wx_data[i]["lo"] +
            "</td></tr>";
    }
}
var data = document.getElementById("data");
//data.textContent = "Average high: " + avg.hi + " Average low: " + avg.lo + " Overall average: " + (parseFloat(avg.hi) + parseFloat(avg.lo)) / 2;
data.innerHTML = "<p><b>Average high:</b> " + avg.hi + "</p><p><b>Average low:</b> " + avg.lo + "</p><p><b>Overall average:</b> " + (parseFloat(avg.hi) + parseFloat(avg.lo)) / 2 + "</p>";